package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MiniDuckSimulator {

    public static void main(String[] args) {

        FlyBehavior flyNoWay = new FlyNoWay();
        FlyBehavior flyWithWings = new FlyWithWings();
        FlyBehavior flyRocketPowered = new FlyRocketPowered();
        QuackBehavior quack = new Quack();
        QuackBehavior mute = new MuteQuack();
        QuackBehavior squeak = new Squeak();


        Duck mallard = new MallardDuck(flyWithWings, quack);
        mallard.performFly();
        mallard.performQuack();

        Duck model = new ModelDuck(flyRocketPowered, mute);
        model.swim();
        model.performFly();
        model.display();
        model.setFlyBehavior(flyNoWay);
        model.performFly();

    }
}
