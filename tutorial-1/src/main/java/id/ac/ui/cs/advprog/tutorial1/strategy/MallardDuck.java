package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    public MallardDuck(FlyBehavior flybehavior, QuackBehavior quackbehavior) {
        super(flybehavior, quackbehavior);
    }

    public void display() {
        System.out.println("I'm a mallard duck");
    }
}
