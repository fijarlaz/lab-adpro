package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
    public ModelDuck(FlyBehavior flybehavior, QuackBehavior quackbehavior) {
        super(flybehavior, quackbehavior);
    }

    public void display() {
        System.out.println("I'm just a model duck");
    }
}
